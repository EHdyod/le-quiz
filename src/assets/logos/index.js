import gitlab from "./gitlab-logo.svg";
import vitejs from "./vitejs-logo.svg";
import vuejs from "./vuejs-logo.svg";
import tailwindcss from "./tailwind-logo.svg";
import pinia from "./pinia-logo.svg";

export const logos = {
    "Gitlab-logo" : gitlab,
    "ViteJS-logo" : vitejs,
    "VueJS-logo" : vuejs,
    "TailwindCSS-logo" : tailwindcss,
    "Pinia-logo" : pinia
}
